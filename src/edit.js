/**
 * Retrieves the translation of text.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-i18n/
 */
import {__} from '@wordpress/i18n';

/**
 * React hook that is used to mark the block wrapper element.
 * It provides all the necessary props like the class name.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/packages/packages-block-editor/#useblockprops
 */
import {useBlockProps} from '@wordpress/block-editor';

/**
 * Lets webpack process CSS, SASS or SCSS files referenced in JavaScript files.
 * Those files can contain any CSS code that gets applied to the editor.
 *
 * @see https://www.npmjs.com/package/@wordpress/scripts#using-css
 */
import './editor.scss';

import {TextControl, Button, PanelBody, IconButton} from '@wordpress/components';
import {RichText} from '@wordpress/block-editor';
import {Fragment} from '@wordpress/element';
import xMark from "./xmark.svg";
import pMark from "./plus.svg";

/**
 * The edit function describes the structure of your block in the context of the
 * editor. This represents what the editor will render when the block is used.
 *
 * @see https://developer.wordpress.org/block-editor/reference-guides/block-api/block-edit-save/#edit
 *
 * @return {WPElement} Element to render.
 */
export default function Edit({attributes, setAttributes}) {

	const handleAddAccordion = () => {
		const accordions = [...attributes.accordions];
		accordions.push({
			header: '',
			content: '',
		});
		setAttributes({accordions});
	};
	const handleRemoveAccordion = (index) => {
		const accordions = [...attributes.accordions];
		accordions.splice(index, 1);
		setAttributes({accordions});
	};

	const handleAccordionHeaderChange = (header, index) => {
		const accordions = [...attributes.accordions];
		accordions[index].header = header;
		setAttributes({accordions});
	};

	const handleAccordionDescriptionChange = (content, index) => {
		const accordions = [...attributes.accordions];
		accordions[index].content = content;
		setAttributes({accordions});
	};

	let accordionFields;

	if (attributes.accordions.length) {
		accordionFields = attributes.accordions.map((accordion, index) => {
			return <div className="accordionItem open" key={index}>
				<span className="accordionItemIndex">{index > 9 ? index + 1 : "0" + (index + 1)}</span>
				<div>
					<i className="accordionIcon opened" style={{backgroundImage: `url(${xMark})`}}
					   onClick={() => handleRemoveAccordion(index)}></i>
					<RichText
						tagName="h2"
						className="accordionItemHeading"
						value={attributes.accordions[index].header}
						onChange={(value) => handleAccordionHeaderChange(value, index)}
						placeholder={__('Accordion title')}
					/>
					<RichText
						tagName="div"
						className="accordionItemContent"
						value={attributes.accordions[index].content}
						onChange={(value) => handleAccordionDescriptionChange(value, index)}
						placeholder={__('Accordion description')}
					/>
				</div>
			</div>;
		});
	} else {
		handleAddAccordion();
	}

	return [
		<div {...useBlockProps()}>
			<div className="accordionWrapper">
				{accordionFields}
			</div>
			<i className="accordionIcon closed" style={{backgroundImage: `url(${pMark})`}}
			   onClick={handleAddAccordion.bind(this)}></i>
		</div>
	];
}
